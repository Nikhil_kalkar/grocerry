import { Component, OnInit } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ProductService } from './provider/product/product.service';
import { Router } from '@angular/router';
// import { homedir } from 'os';
import { FCM } from '@ionic-native/fcm/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'Home',
      url: 'home',
      icon: 'mail'
    },
    {
      title: 'Product',
      url: 'product',
      icon: 'mail'
    },
    {
      title: 'Profile',
      url: 'profile',
      icon: 'mail'
    },
    {
      title: 'logout',
      url: 'login',
      icon: 'mail'
    },
    {
      title: 'address',
      url: 'addresslist',
      icon: 'mail'
    },
    {
      title: ' My orders',
      url: 'myorder',
      icon: 'mail'
    }

    // {
    //   title: 'Inbox',
    //   url: '/folder/Inbox',
    //   icon: 'mail'
    // },
    // {
    //   title: 'Outbox',
    //   url: '/folder/Outbox',
    //   icon: 'paper-plane'
    // },
    // {
    //   title: 'Favorites',
    //   url: '/folder/Favorites',
    //   icon: 'heart'
    // },
    // {
    //   title: 'Archived',
    //   url: '/folder/Archived',
    //   icon: 'archive'
    // },
    // {
    //   title: 'Trash',
    //   url: '/folder/Trash',
    //   icon: 'trash'
    // },
    // {
    //   title: 'Spam',
    //   url: '/folder/Spam',
    //   icon: 'warning'
    // }
  ];
  public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public productservice: ProductService,
    public router: Router, public navctrl: NavController,
    private fcm: FCM
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      // subscribe to a topic
      // this.fcm.subscribeToTopic('Deals');

      // get FCM token
      this.fcm.getToken().then(token => {
        console.log(token);
      });

      // ionic push notification example
      this.fcm.onNotification().subscribe(data => {
        console.log(data);
        if (data.wasTapped) {
          console.log('Received in background');
        } else {
          console.log('Received in foreground');
        }
      });      

      // refresh the FCM token
      this.fcm.onTokenRefresh().subscribe(token => {
        console.log(token);
      });

      // unsubscribe from a topic
      // this.fcm.unsubscribeFromTopic('offers');
    });
  }

  ngOnInit() {

    let uId = localStorage.getItem('user_id');
    if (uId) {
      this.navctrl.navigateRoot('/home');
    } else {
      const path = window.location.pathname.split('folder/')[1];
      console.log(path);
      if (path !== undefined) {
        this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
        console.log(this.selectedIndex);
      }
    }
  }

  goto(x) {
    console.log(x);
    if (x.title == 'Product') {
      this.productservice.title = x.title;
    }
    this.router.navigateByUrl(x.url);
  }
}
