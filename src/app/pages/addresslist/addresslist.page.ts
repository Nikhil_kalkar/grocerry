import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addresslist',
  templateUrl: './addresslist.page.html',
  styleUrls: ['./addresslist.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AddresslistPage implements OnInit {
  addresslist = [
    {
      id: 0, name: "vhs,dfskdf,sdkfnsdlkf, pachora, Tal:pachora, Dist: Jalgaon, 424201", lat: 21.656498, long: 25.265546
    },
    {
      id: 1, name: "vhs,dfskdf,sdkfnsdlkf, pachora, Tal:pachora, Dist: Jalgaon, 424201", lat: 21.656498, long: 25.265546
    },
    {
      id: 3, name: "vhs,dfskdf,sdkfnsdlkf, pachora, Tal:pachora, Dist: Jalgaon, 424201", lat: 21.656498, long: 25.265546
    }
  ]
  constructor(public router: Router) { }

  ngOnInit() {
    console.log(this.addresslist)
  }
  addnewadd(){
    this.router.navigateByUrl('/address');
  }
  Edit(data) {
    console.log(data);
    this.router.navigateByUrl('/address');
  }
  delet(data, index) {
    console.log(data, index);
    this.addresslist.splice(index, 1);
  }
}
