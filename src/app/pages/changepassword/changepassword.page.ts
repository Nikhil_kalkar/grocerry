import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { LoaderService } from 'src/app/provider/loader/loader.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.page.html',
  styleUrls: ['./changepassword.page.scss'],
})
export class ChangepasswordPage implements OnInit {
  forgotpassForm: FormGroup
  constructor(private fb : FormBuilder, private loderservice: LoaderService, public router: Router) { }

  ngOnInit() {
    this.createforgotform();
  }
  createforgotform(){
    this.forgotpassForm = this.fb.group({
      email: ['', Validators.compose([Validators.required])]
    })
  }
  onsubmit(){
    if(this.forgotpassForm.valid){
      this.loderservice.presentLoading('Please wait...');
      this.router.navigateByUrl('login');
      this.loderservice.dismissLoader();
    } else {
      this.validateAllFormFields(this.forgotpassForm);
    }
  }
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}
