import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { CartService } from 'src/app/provider/cart/cart.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.page.html',
  styleUrls: ['./checkout.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CheckoutPage implements OnInit {
  addresslist = [
    {
      id: 0, name: "vhs,dfskdf,sdkfnsdlkf, pachora, Tal:pachora, Dist: Jalgaon, 424201", lat: 21.656498, long: 25.265546
    },
    {
      id: 1, name: "vhs,dfskdf,sdkfnsdlkf, pachora, Tal:pachora, Dist: Jalgaon, 424201", lat: 21.656498, long: 25.265546
    },
    {
      id: 3, name: "vhs,dfskdf,sdkfnsdlkf, pachora, Tal:pachora, Dist: Jalgaon, 424201", lat: 21.656498, long: 25.265546
    }
  ]
  subtotal: any;
  totaltax: any;
  cartitem: any;
  week: any;
  days: any = [{
    name: ''
  }];
  slots=[
    { day :0 , slot :[
      { id: 0, time: '9 to 12'},
      { id: 1, time: '12to 3'},
    ]},
    { day :1 , slot :[
      { id: 0, time: '9 to 12'},
      { id: 1, time: '12 to 3'},
      { id: 2, time: '3 to 6'},
      { id: 3, time: '6 to 9'},
    ]},
    { day :2 , slot :[
      { id: 0, time: '9 to 12'},
      { id: 1, time: '12 to 3'},
      { id: 2, time: '3 to 6'},
      { id: 3, time: '6 to 9'},
    ]},
    { day :3 , slot :[
      { id: 0, time: '9 to 12'},
      { id: 1, time: '12 to 3'},
      { id: 2, time: '2 to 6'},
      { id: 3, time: '6 to 9'},
    ]},
    { day :4 , slot :[
      { id: 0, time: '9 to 12'},
      { id: 1, time: '12 to 3'},
      { id: 2, time: '3 to 6'},
      // { id: 3, time: '6 to 9'},
    ]},
    { day :5 , slot :[
      { id: 0, time: '9 to 12'},
      { id: 1, time: '12 to 3'},
      { id: 2, time: '3 to 6'},
      { id: 3, time: '6 to 9'},
    ]},
    { day :6 , slot :[
      { id: 0, time: '9 to 12'},
      { id: 1, time: '12 to 3'},
      { id: 2, time: '3 to 6'},
      { id: 3, time: '6 to 9'},
    ]},
]
showslots:any;
  constructor(public router: Router, public cart: CartService) { }

  ngOnInit() {
    this.subtotal = this.cart.subtotal;
    this.totaltax = this.cart.totaltax;
    this.cartitem = this.cart.cartitem;
    this.chechdate()
  }
  chechdate() {
    var date = new Date();
    this.week = []
    for (let i = 0; i < 7; i++) {
      date.setDate(date.getDate() + 1);
      // var finalDate = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
      var finalDate = date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate()
      var mydate = new Date(finalDate);
      console.log(finalDate);
      console.log(mydate);
      this.week.push({ "day": mydate, "selected": '' });
    }
    this.selectedday(this.week[0], 1);
    this.selectslot(this.week[0], 1);
  }
  selectedday(data, index){
    for (let i = 0; i < this.week.length; i++) {
      if(this.week[i].day == data.day){
        this.week[i].selected = true;
      } else{
        this.week[i].selected = false;
      }
    }
  }
  selectslot(data, index){
    var d = new Date(data.day).getDay();
    console.log(d);
    for(let i=0; i< this.slots.length; i++){
      if(d == this.slots[i].day){
        this.showslots = this.slots[i].slot;
      }
    }
  }
  Edit(data) {
    console.log(data);
    this.router.navigateByUrl('/address');
  }
  delet(data, index) {
    console.log(data, index);
    this.addresslist.splice(index, 1);
  }
  radioGroupChange(event) {
    console.log("radioGroupChange", event.detail);
    // this.selectedRadioGroup = event.detail;
  }

  radioFocus() {
    console.log("radioFocus");
  }
  radioSelect(event) {
    console.log("radioSelect", event.detail);
    // this.selectedRadioItem = event.detail;
  }
  radioBlur() {
    console.log("radioBlur");
  }

  placeorder(){
    this.router.navigateByUrl('home');
  }
}
