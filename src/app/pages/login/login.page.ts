import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
// import { LoadingComponent } from 'src/app/components/loading/loading.component';
import { LoaderService } from 'src/app/provider/loader/loader.service';
import { NavController } from '@ionic/angular';
import { LoginService } from 'src/app/provider/login/login.service';
import { Global } from '../../provider/config';
import { DashboardService } from 'src/app/provider/dashboard/dashboard.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  public type = 'password';
  public showPass = false;
  user = {
    mobile: '',
    password: '',
    rememberme: false
  }
  constructor(private fb: FormBuilder, private router: Router, private loginservice: LoginService,
    public loadservice: LoaderService, public navctrl: NavController, private dash:DashboardService) { }

  ngOnInit() {
    let userLogindetal = JSON.parse(localStorage.getItem('userLoginForm'));
    if (userLogindetal) {
      this.addloginform(userLogindetal);
    } else {
      this.addloginform(this.user);
    }
  }
  addloginform(data) {
    this.loginForm = this.fb.group({
      admin_id: Global.admin_id,
      mobile: [data.mobile, Validators.compose([Validators.required])],
      password: [data.password, Validators.compose([Validators.required])],
      rememberme: [data.rememberme]
    })
  }
  showPassword() {
    this.showPass = !this.showPass;
    if (this.showPass) {
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }
  gotoforgot() {
    this.router.navigateByUrl('/forgotpassword');
  }
  newregister() {
    this.router.navigateByUrl('/register');
  }
  onlogin() {
    console.log(this.loginForm);
    if (this.loginForm.valid) {
      this.loadservice.presentLoading('Please wait...');
      if (this.loginForm.value.rememberme) {
        localStorage.setItem('userLoginForm', JSON.stringify(this.loginForm.value));
        
        // this.router.navigateByUrl('/home');
      } else {
        localStorage.removeItem('userLoginForm');
      }
      console.log(this.loginForm.value);
      this.loginservice.userLogin(this.loginForm.value).then(res=>{
        console.log(res);
        if(res['status'] == "success"){
          localStorage.setItem('user_id', res['message'].users_id); 
          this.loadservice.presentToast('Login Successfull');
          // this.toastr.presentToast('Login Successfull');
          this.navctrl.navigateRoot('/home');
        } else{
          this.loadservice.presentToast(res['message']);
        }
        this.loadservice.dismissLoader();
      }, err=>{
        this.loadservice.dismissLoader();
        this.loadservice.presentToast(err);
      })
     
    
    } else {
      this.validateAllFormFields(this.loginForm);
    }
  }
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

 
}
