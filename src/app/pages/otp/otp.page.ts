import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { LoaderService } from 'src/app/provider/loader/loader.service';
import { Router } from '@angular/router';
declare var SMS: any;
declare var document: any;
@Component({
  selector: 'app-otp',
  templateUrl: './otp.page.html',
  styleUrls: ['./otp.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class OTPPage implements OnInit {
  @ViewChild('input1') inputPin: any;
  @ViewChild('input2') inputPin1: any;
  @ViewChild('input3') inputPin2: any;
  @ViewChild('input4') inputPin3: any;
  @ViewChild('input5') inputPin4: any;
  @ViewChild('input6') inputPin5: any;

  otpForm: FormGroup;
  otp: any;
  constructor(private fb: FormBuilder, public loadservice: LoaderService, private router: Router, ) { }

  ngOnInit() {
    // this.receiveSMS();
    this.createOtpForm();
    // this.startArchiveSMS();

  }
  receiveSMS() {

    if (SMS) SMS.startWatch(function () {
      console.log('watching started');
    }, function () {
      console.log('failed to start watching');
    });
  }
  createOtpForm() {
    this.otpForm = this.fb.group({
      otp1: ["", Validators.required],
      otp2: ["", Validators.required],
      otp3: ["", Validators.required],
      otp4: ["", Validators.required],
      otp5: ["", Validators.required],
      otp6: ["", Validators.required]
    });
  }
  onSubmitForm() {
    console.log(this.otpForm);
    if (this.otpForm.valid) {
      let finalotp = this.otpForm.value.otp1 + "" + this.otpForm.value.otp2 + "" + this.otpForm.value.otp3 + "" + this.otpForm.value.otp4 + "" + this.otpForm.value.otp5 + "" + this.otpForm.value.otp6;
      console.log(finalotp)
      this.loadservice.presentLoading('Verifying please wait..');
      this.router.navigateByUrl('login');
      this.loadservice.dismissLoader();
    } else {
      this.validateAllFormFields(this.otpForm);
    }
  }
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
  startArchiveSMS() {
    let thisref = this;
    document.addEventListener('onSMSArrive', function (e) {
      console.log(e);
      var sms = e['data'];
      // console.log("received sms " + JSON.stringify(sms));
      if (sms.address == this.smsaddress) //look for your message address
      {
        thisref.otp = sms.body.substr(42, 4);
        console.log(sms.body.substr(42, 4));

        thisref.stopSMS();
        thisref.setOtp(thisref.otp);
      }
    });
  }
  stopSMS() {
    if (SMS) SMS.stopWatch(function () {
      console.log('watching stopped');
    }, function () {
      console.log('failed to stop watching');
    });
  }

  setOtp(receivedotp: any) {

    this.otpForm.controls['otp1'].setValue(receivedotp.substr(0, 1));
    this.otpForm.controls['otp2'].setValue(receivedotp.substr(1, 1));
    this.otpForm.controls['otp3'].setValue(receivedotp.substr(2, 1));
    this.otpForm.controls['otp4'].setValue(receivedotp.substr(3, 1));
    this.otpForm.controls['otp5'].setValue(receivedotp.substr(4, 1));
    this.otpForm.controls['otp6'].setValue(receivedotp.substr(5, 1));
  }

  focusInput(input,input1,input2,current){
    console.log(input);
    if(current=='input1'){
      if(input1.value!=null && input1.value!=""){
        this.inputPin1.setFocus();
      }
      else{
        this.inputPin.setFocus();
      }
    }
    if(current=='input2'){
      if(input1.value!=null && input1.value!=""){
        this.inputPin2.setFocus();
      }
      else{
        this.inputPin.setFocus();
      }
    }
    if(current=='input3'){
      if(input1.value!=null && input1.value!=""){
        this.inputPin3.setFocus();
      }
      else{
        this.inputPin1.setFocus();
      }
    }
    if(current=='input4'){
      if(input1.value!=null && input1.value!=""){
        this.inputPin4.setFocus();
      }
      else{
        this.inputPin2.setFocus();
      }
    }
    if(current=='input5'){
      if(input1.value!=null && input1.value!=""){
        this.inputPin5.setFocus();
      }
      else{
        this.inputPin3.setFocus();
      }
    }
    if(current=='input6'){
      if(input1.value!=null && input1.value!=""){
        this.inputPin5.setFocus();
      }
      else{
        this.inputPin4.setFocus();
      }
    }
  }
  resendotp(){
    console.log("resend otp");
  }
}
