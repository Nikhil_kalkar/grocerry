import { Component, OnInit, ViewEncapsulation} from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { LoaderService } from 'src/app/provider/loader/loader.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RegisterPage implements OnInit {
  registerForm: FormGroup;
  public type = 'password';
  public showPass = false;
  public type1 = 'password';
  public showPass1 = false;
  constructor(private fb: FormBuilder, public loadservice: LoaderService, private router: Router,) { }

  ngOnInit() {
    this.addregisterform();
  }
  gotologin(){
    window.history.back();
  }
  addregisterform(){
    this.registerForm = this.fb.group({
      name: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.required])],
      mobile: ['', Validators.compose([Validators.required])],
      pass: ['', Validators.compose([Validators.required])],
      confirmpass: ['', Validators.compose([Validators.required])],
    })
  }
  onsubmit(){
    console.log(this.registerForm.value);
    if(this.registerForm.valid){
      this.loadservice.presentLoading('Please wait...');
      this.router.navigateByUrl('/otp');
      this.loadservice.dismissLoader();
    } else{
      this.validateAllFormFields(this.registerForm);
    }
  }
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
  showPassword() {
    this.showPass = !this.showPass;
    if (this.showPass) {
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }
  showPassword1() {
    this.showPass1 = !this.showPass1;
    if (this.showPass1) {
      this.type1 = 'text';
    } else {
      this.type1 = 'password';
    }
  }
}
