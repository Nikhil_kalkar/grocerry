import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Global } from './../config';
@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  private headers = new HttpHeaders();
  constructor(public http: HttpClient,) { }

  get_cat_list(){
    return this.http.get( Global.categories + "?admin_id=" + Global.admin_id)
    .toPromise()
    .then(response => {
    return response = response; 
    }, err=>{
      return err;
    })
  }
}
