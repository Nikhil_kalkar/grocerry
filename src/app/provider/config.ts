export class Global {

    public static admin_id = "557167cc5a";

    public static URL = "http://api.d-lightweb.com/";
    public static Urldomain = "http://grocery.d-lightweb.com/";
    public static banurl = "uploads/banners/resizes/";
    public static caturl = "uploads/categories/resizes/";
    public static produrl = "uploads/product/resizes/";
    /*********login*************/
    public static LOGIN_URL = Global.URL + 'Login_api/auth_login';
    /************* Dashboard *********/
    public static DASHBOARD = Global.URL + 'Dashboard_api/dashboard_list';
    /********** category *********/
    public static categories = Global.URL + 'Categories_api/all_cat_list';
    public static subcategories = Global.URL + 'Categories_api/cat_subcat_list';
    /******* product ********/
    public static allproductlist =  Global.URL + 'Products_api/all_product_list';
    public static productdetail =  Global.URL + 'Products_api/product_details';
    public static catwiseproduct = Global.URL + 'Categories_api/cat_product_list';

}