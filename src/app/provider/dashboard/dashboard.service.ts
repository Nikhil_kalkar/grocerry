import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Global } from './../config';
@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  private headers = new HttpHeaders();
  constructor(public http: HttpClient,) { }
  // get_dash_data(){
  //   return new Promise(resolve => {
  //     this.http.get(Global.DASHBOARD, Global.admin_id)
  //       .subscribe(data => {
  //         console.log(data)
  //         // let userdata = JSON.stringify(data);
  //         resolve(data);
  //       },
  //         err => {
  //           resolve(err);
  //           if (err) {
  //             // this.toastr.error('Please check server connection', 'Server Error!');
  //           }
  //         });
  //   });
  // }

  get_dash_data() : Promise<any> {
    return this.http.get( Global.DASHBOARD + "?admin_id=" + Global.admin_id)
    .toPromise()
    .then(response => {
    return response = response; 
    }, err=>{
      return err;
    })
  }


  // private handleError(error: Response | any) {
  //   return Promise.reject(error.message || error);
  // }
}
