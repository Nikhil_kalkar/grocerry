import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { catchError, tap, map } from 'rxjs/operators';
import { HttpClient, HttpClientModule, HttpHeaders, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Global } from './../config';
// import { Http, Response, RequestOptions, Headers } from '@angular/http';

// const httpOptions = {
//   headers: new HttpHeaders({ 'Content-Type': 'application/json' })
// };

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private headers = new HttpHeaders();
  constructor(public http: HttpClient,) { }

  // login(param){
  //   return new Promise(resolve => {
  //     console.log(Global.LOGIN_URL);
  //     this.http.post(Global.LOGIN_URL, param)
  //       .subscribe(data => {
  //         console.log(data)
  //         resolve(data);
  //       },
  //         err => {
  //           resolve(err);
  //           if (err) {
  //           }
  //         });
  //   });
  // }

  public userLogin(a: any): Promise<any> {
    let url = "http://api.d-lightweb.com/Login_api/auth_login"
    let body: string = '&admin_id=' + a.admin_id + '&mobile=' + a.mobile + '&password=' + a.password
    return this.http.post(url, body, {  headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8'),})
      .toPromise()
      .then(response => {
        return response = response;
      })
      .catch(this.handleError);
    }

  // login(data: any): Observable<any> {
  //   console.log("nikhil");
  //   // this.url = this.baseURL + apiUrl;
  //   let dta = {
  //       admin_id : '557167cc5a',
  //       mobile : '8983194806',
  //       password : '123'
  //   }
  //   return this.http.post(Global.LOGIN_URL, dta, httpOptions)
  //     .pipe(
  //       map((response => response)),
  //       catchError(this.handleError)
  //     );
  // }
  private handleError(error: Response | any) {
    return Promise.reject(error.message || error);
  }
}
